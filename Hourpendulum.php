<?php


class HourpendulumPlugin extends MantisPlugin
{
    var $skip = false;

    function register()
    {
        $this->name = plugin_lang_get( 'title' );
        $this->description = plugin_lang_get( 'description' );
        $this->page = 'config_page';
        $this->version = '1.0';
        $this->requires = ['MantisCore' => '2.0.0'];
        $this->author = 'Praful Zalke';
        $this->contact = 'praful.zalke@gmail.com';
        $this->url = '';
    }

    function install()
    {
        if (version_compare(PHP_VERSION, '5.3.0', '<')) {
            plugin_error(ERROR_PHP_VERSION, ERROR);
            return false;
        }
        if (!extension_loaded('curl')) {
            plugin_error(ERROR_NO_CURL, ERROR);
            return false;
        }
        return true;
    }

    function config() {
        return array(
            'url_webhooks' => array(),
            'url_webhook' => '',
            'bot_name' => 'mantis',
            'bot_icon' => '',
            'skip_private' => true,
            'skip_bulk' => true,
            'link_names' => false,
            'usernames' => array(),
            'columns' => array(
                'id',
                'project_id',
                'reporter_id',
                'status',
                'handler_id',
                'target_version',
                'priority',
                'severity',
                'duplicate_id',
                'reproducibility',
                'resolution',
                'projection',
                'category_id',
                'eta',
                'view_state',
                'summary',
                'last_updated',
                'date_submitted',
                'due_date',
                'description',
                'steps_to_reproduce',
                'additional_information',
            ),
        );
    }

    function hooks() {
        return array(
            'EVENT_REPORT_BUG' => 'bug_report',
            'EVENT_UPDATE_BUG' => 'bug_update',
            'EVENT_BUG_DELETED' => 'bug_deleted',
            'EVENT_BUG_ACTION' => 'bug_action',
            'EVENT_BUGNOTE_ADD' => 'bugnote_add_edit',
            'EVENT_BUGNOTE_EDIT' => 'bugnote_add_edit',
            'EVENT_BUGNOTE_DELETED' => 'bugnote_deleted',
            'EVENT_BUGNOTE_ADD_FORM' => 'bugnote_add_form',
        );
    }

    function skip_private($bug_or_note) {
        return (
            $bug_or_note->view_state == VS_PRIVATE &&
            plugin_config_get('skip_private')
        );
    }

    function bugnote_add_form($event, $bug_id)
    {
        if ($_SERVER['PHP_SELF'] !== '/bug_update_page.php') return;
        echo '<tr>';
        echo '<th class="category">' . plugin_lang_get( 'skip' ) . '</th>';
        echo '<td colspan="5">';
        echo '<label>';
        echo '<input ', helper_get_tab_index(), ' name="hourpendulum_skip" class="ace" type="checkbox" />';
        echo '<span class="lbl"></span>';
        echo '</label>';
        echo '</td></tr>';
    }

    function bug_report_update($event, $bug, $bug_id)
    {
        $this->skip = $this->skip || gpc_get_bool('hourpendulum_skip') || $this->skip_private($bug);
        $data = $this->getPostData($bug);
        $data['event'] = $event;
        $data['id'] = $bug_id;
        $this->notify($data, $this->get_webhook());
    }

    function bug_report($event, $bug, $bug_id)
    {
        $this->bug_report_update($event, $bug, $bug_id);
    }

    function bug_update($event, $bug_existing, $bug_updated)
    {
        $this->bug_report_update($event, $bug_updated, $bug_updated->id);
    }

    function bug_action($event, $action, $bug_id)
    {
        $this->skip = $this->skip || gpc_get_bool('hourpendulum_skip') || plugin_config_get('skip_bulk');

        if ($action !== 'DELETE') {
            $bug = bug_get($bug_id);
            $this->bug_report_update('EVENT_UPDATE_BUG', $bug, $bug_id);
        }
    }

    function bug_deleted($event, $bug_id)
    {
        $bug = bug_get($bug_id);
        $this->skip = $this->skip || gpc_get_bool('hourpendulum_skip') || $this->skip_private($bug) ;
        $data = $this->getPostData($bug);
        $data['event'] = $event;
        $data['bug_event'] = 'bug_deleted';
        $data['id'] = $bug_id;
        $this->notify($data, $this->get_webhook());
    }

    function bugnote_add_edit($event, $bug_id, $bugnote_id)
    {
        $bug = bug_get($bug_id);
        $bugnote = bugnote_get($bugnote_id);
        $this->skip = $this->skip || gpc_get_bool('hourpendulum_skip') || $this->skip_private($bug) || $this->skip_private($bugnote);
        $url = string_get_bugnote_view_url_with_fqdn($bug_id, $bugnote_id);
        $note = bugnote_get_text($bugnote_id);
        $data = $this->getPostData($bug);
        $data['event'] = $event;
        $data['id'] = $bug_id;
        $data['note'] = $note;
        $data['url'] = $url;
        $this->notify($data, $this->get_webhook());
    }

    function get_text_attachment($text)
    {
        $attachment = array('color' => '#3AA3E3', 'mrkdwn_in' => array('pretext', 'text', 'fields'));
        $attachment['fallback'] = text . "\n";
        $attachment['text'] = $text;
        return $attachment;
    }

    function bugnote_deleted($event, $bug_id, $bugnote_id)
    {
        $bug = bug_get($bug_id);
        $bugnote = bugnote_get($bugnote_id);
        $this->skip = $this->skip || gpc_get_bool('hourpendulum_skip') || $this->skip_private($bug) || $this->skip_private($bugnote);

        $url = string_get_bug_view_url_with_fqdn($bug_id);
        $data = $this->getPostData($bug);
        $data['event'] = $event;
        $data['id'] = $bug_id;
        $data['url'] = $url;
        $this->notify($data, $this->get_webhook());
    }

    function format_summary($bug)
    {
        $summary = bug_format_id($bug->id) . ': ' . string_display_line_links($bug->summary);
        return strip_tags(html_entity_decode($summary));
    }

    function format_text($bug, $text) {
        $t = string_display_line_links($this->bbcode_to_hourpendulum($text));
        return strip_tags(html_entity_decode($t));
    }

    function get_attachment($bug)
    {
        $data = [];
        $t_columns = (array)plugin_config_get('columns');
        foreach ($t_columns as $t_column) {
            $title = column_get_title( $t_column );
            if (property_exists($bug, $t_column)) {
                $data[$title] = $bug->$t_column;
            }
        }
        $data['reporter'] = $this->get_user_name(auth_get_current_user_id());
        return $data;
    }


    function getPostData($bug)
    {
        $data = [];
        $t_columns = (array)plugin_config_get('columns');

        foreach ($t_columns as $t_column) {
            if (property_exists($bug, $t_column)) {
                $data[$t_column] = $bug->$t_column;
            }
        }

        $project = project_get_name($bug->project_id);
        $url = string_get_bug_view_url_with_fqdn($bug->id);
        $summary = $this->format_summary($bug);
        $data['reporter'] = $this->get_user_name(auth_get_current_user_id());
        $data['project'] = $project;
        $data['url'] = $url;
        $data['summary'] = $summary;

        return $data;
    }

    function get_webhook($project = null)
    {
        $webhooks = plugin_config_get('url_webhooks');
        return array_key_exists($project, $webhooks) ? $webhooks[$project] : plugin_config_get('url_webhook');
    }

    function notify($data, $webhook)
    {
        if ($this->skip) return;
        if (empty($webhook)) return;

        $ch = curl_init();
        $url = sprintf('%s', trim($webhook));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($httpCode !== 201) {
            trigger_error(curl_errno($ch) . ': ' . curl_error($ch), E_USER_WARNING);
            plugin_error('ERROR_CURL', E_USER_ERROR);
        }
        curl_close($ch);
    }

    function bbcode_to_hourpendulum($bbtext)
    {
        $bbtags = array(
            '[b]' => '*','[/b]' => '* ',
            '[i]' => '_','[/i]' => '_ ',
            '[u]' => '_','[/u]' => '_ ',
            '[s]' => '~','[/s]' => '~ ',
            '[sup]' => '','[/sup]' => '',
            '[sub]' => '','[/sub]' => '',

            '[list]' => '','[/list]' => "\n",
            '[*]' => '• ',

            '[hr]' => "\n———\n",

            '[left]' => '','[/left]' => '',
            '[right]' => '','[/right]' => '',
            '[center]' => '','[/center]' => '',
            '[justify]' => '','[/justify]' => '',
        );

        $bbtext = str_ireplace(array_keys($bbtags), array_values($bbtags), $bbtext);

        $bbextended = array(
            "/\[code(.*?)\](.*?)\[\/code\]/is" => "```$2```",
            "/\[color(.*?)\](.*?)\[\/color\]/is" => "$2",
            "/\[size=(.*?)\](.*?)\[\/size\]/is" => "$2",
            "/\[highlight(.*?)\](.*?)\[\/highlight\]/is" => "$2",
            "/\[url](.*?)\[\/url]/i" => "<$1>",
            "/\[url=(.*?)\](.*?)\[\/url\]/i" => "<$1|$2>",
            "/\[email=(.*?)\](.*?)\[\/email\]/i" => "<mailto:$1|$2>",
            "/\[img\]([^[]*)\[\/img\]/i" => "<$1>",
        );

        foreach($bbextended as $match=>$replacement){
            $bbtext = preg_replace($match, $replacement, $bbtext);
        }
        $bbtext = preg_replace_callback("/\[quote(=)?(.*?)\](.*?)\[\/quote\]/is",
            function ($matches) {
                if (!empty($matches[2]))
                    $result = "\n> _*" . $matches[2] . "* wrote:_\n> \n";
                $lines = explode("\n", $matches[3]);
                foreach ($lines as $line)
                    $result .= "> " . $line . "\n";
                return $result;
            }, $bbtext);
        return $bbtext;
    }

    function get_user_name($user_id)
    {
        $user = user_get_row($user_id);
        $username = $user['username'];
        $usernames = plugin_config_get('usernames');
        $username = array_key_exists($username, $usernames) ? $usernames[$username] : $username;
        return $username;
    }
}
