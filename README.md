MantisBT-Hourpendulum
==============

A [MantisBT](http://www.mantisbt.org/) plugin to send bug updates to [Hourpendulum](https://hourpendulum.com/).


# Setup
* The `master` branch requires Mantis 2.0.x, while the `master-1.2.x` branch works for Mantis 1.2.x.
* Extract this repo to your *Mantis folder/plugins/Hourpendulum*.
* On the Hourpendulum side, add a new "Incoming Webhooks" integration and note the URL that Hourpendulum generates for you.
* On the MantisBT side, access the plugin's configuration page and fill in your Hourpendulum webhook URL.
* You can specify which bug fields appear in the Hourpendulum notifications. Edit the *plugin_Hourpendulum_columns* configuration option for this purpose.  Follow the instructions on the plugin configuration page.
