<?php


form_security_validate( 'plugin_Hourpendulum_config' );
access_ensure_global_level( config_get( 'manage_plugin_threshold' ) );

/**
 * Sets plugin config option if value is different from current/default
 * @param string $p_name  option name
 * @param string $p_value value to set
 * @return void
 */
function config_set_if_needed( $p_name, $p_value ) {
	if ( $p_value != plugin_config_get( $p_name ) ) {
		plugin_config_set( $p_name, $p_value );
	}
}

$t_redirect_url = plugin_page( 'config_page', true );
layout_page_header( null, $t_redirect_url );
layout_page_begin();

if (gpc_get_string( 'url_webhook_test', false )) {

  plugin_get()->notify(
    plugin_lang_get('url_webhook_test_text'),
    gpc_get_string( 'url_webhook' )
  );

}

config_set_if_needed( 'url_webhook' , gpc_get_string( 'url_webhook' ) );
config_set_if_needed( 'bot_name' , gpc_get_string( 'bot_name' ) );
config_set_if_needed( 'bot_icon' , gpc_get_string( 'bot_icon' ) );
config_set_if_needed( 'skip_private' , gpc_get_bool( 'skip_private' ) );
config_set_if_needed( 'skip_bulk' , gpc_get_bool( 'skip_bulk' ) );
config_set_if_needed( 'link_names' , gpc_get_bool( 'link_names' ) );

form_security_purge( 'plugin_Hourpendulum_config' );

html_operation_successful( $t_redirect_url );
layout_page_end();
